
# Idea 1 - make this an angular constant
# Idea 2 - make this a service, returning a random quote

quotes = [
   * text: "Look behind you, a Three-Headed Monkey!"
     author: "Guybrush Threepwood"
   * text: "[singing] Oh... there's... a... monkey in my pocket / And he's stealing all my change / His stare is blank and glassy / I suspect that he's deranged!"
     author: "Guybrush Threepwood"
   * text: "\"Ask me about Grim Fandango.\" I don't want people asking me about Grim Fandango."
     author: "Guybrush Threepwood"
   * text: "You know... I don't think my father would approve of me dating the undead, and you're probably too nice a zombie-pirate for me anyway. Let's just be friends instead."
     author: "Elaine Marley"
   * text: "Now with the demon flames of this voodoo cannonball, I'll blast my significant other into the significant otherworld, ha ha! That'll show how much I truly care."
     author: "LeChuck"
   * text: "Aye aye, Captain. Fresh bananas for the whole crew!"
     author: "Mr. Fossey"
   * text: "I am Murray, the invincible demonic skull!"
     author: "Murray"
   * text: "Shut up, or I'll eat you."
     author: "Lemonhead"
   * text: "Ha-ha! Taste cold steel, feeble cannon restraint rope!"
     author: "Guybrush Threepwood"
   * text: "You fool! You gave cheese to a lactose intolerant volcano god!"
     author: "Lemonhead"
   * text: "Arrr! Math be hard! Let's go shopping!"
     author: "LeChuck"
   * text: "Rest in peace, and all that."
     author: "Mort"
   * text: "You can count on me, Wally. Just as soon as I defeat LeChuck, rescue Elaine, set all the monkeys free, and ride the Madly Rotating Buccaneer, I'll come back to release you."
     author: "Guybrush Threepwood"
   * text: "Burn down every island in the Caribbean if you have to, but bring me my bride!... and more slaw! Curse the villains, they never give you enough slaw with these value meals."
     author: "LeChuck"
]
