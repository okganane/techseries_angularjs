module.exports = (grunt) ->
   grunt.initConfig do

      livescript:
         code:
            files: [
             * src: "app.ls"
               dest: "app.js"
             * src: "quotes.ls"
               dest: "quotes.js"
            ]

      watch:
         code:
            files: [ "*.ls" ]
            tasks: [ "livescript" ]
            options:
               spawn: false

   grunt.loadNpmTasks "grunt-livescript"
   grunt.loadNpmTasks "grunt-contrib-watch"

   grunt.registerTask "default", ["livescript"]
