myApp = angular.module "MyApp", []

myApp.controller "QuoteController", ($scope) ->
   $scope.insightful_quote =
      text: "Look behind you, a Three-Headed Monkey!"
      author: "Guybrush Threepwood"
