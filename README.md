# README #

Collection of source code used during the AngularJS Tech Series, submitted by each of the contributors.

### Part 1: Basics ###

Covering the basics of AngularJS, including:
* MVC (Model View Controller pattern)
* $injector basics
* $service & $factory
* $scope.$apply
* basic directives

## Installing ##

* Clone this repo
* From "1-basics" directory, run the following:

1. npm install -g livescript grunt-cli
2. npm install
3. bower install

* To compile and run the project:

1. grunt
2. open index.html in your browser

* You can also have grunt watch for changes and automatically recompile with:

1. grunt watch